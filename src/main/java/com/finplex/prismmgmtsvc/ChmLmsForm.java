package com.finplex.prismmgmtsvc;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(schema = "new_fp_schema_test",name = "chm_lms_form")
public class ChmLmsForm implements Serializable {
    @Id
    @Column(name = "id")
    private Integer id;

    @Lob
    @Column(name = "loan_id")
    private String loanId;

    @Lob
    @Column(name = "first_name")
    private String firstName;

    @Lob
    @Column(name = "middle_name")
    private String middleName;

    @Lob
    @Column(name = "last_name")
    private String lastName;

    @Lob
    @Column(name = "Gender")
    private String gender;

    @Lob
    @Column(name = "date_of_birth")
    private String dateOfBirth;

    @Lob
    @Column(name = "marital_status")
    private String maritalStatus;

    @Column(name = "phone_number")
    private Long phoneNumber;

    @Lob
    @Column(name = "alternate_number1")
    private String alternateNumber1;

    @Lob
    @Column(name = "alternate_number2")
    private String alternateNumber2;

    @Lob
    @Column(name = "email")
    private String email;

    @Lob
    @Column(name = "alternate_email")
    private String alternateEmail;

    @Lob
    @Column(name = "pan_id")
    private String panId;

    @Lob
    @Column(name = "driving_licence_id")
    private String drivingLicenceId;

    @Lob
    @Column(name = "voter_id")
    private String voterId;

    @Lob
    @Column(name = "passport")
    private String passport;

    @Lob
    @Column(name = "ration_card")
    private String rationCard;

    @Lob
    @Column(name = "uid")
    private String uid;

    @Lob
    @Column(name = "other_id1")
    private String otherId1;

    @Lob
    @Column(name = "other_id2")
    private String otherId2;

    @Lob
    @Column(name = "father_name")
    private String fatherName;

    @Lob
    @Column(name = "spouse")
    private String spouse;

    @Lob
    @Column(name = "mothers_name")
    private String mothersName;

    @Lob
    @Column(name = "address1")
    private String address1;

    @Lob
    @Column(name = "village1")
    private String village1;

    @Lob
    @Column(name = "city1")
    private String city1;

    @Lob
    @Column(name = "state1")
    private String state1;

    @Column(name = "pin1")
    private Integer pin1;

    @Lob
    @Column(name = "country1")
    private String country1;

    @Lob
    @Column(name = "address2")
    private String address2;

    @Lob
    @Column(name = "village2")
    private String village2;

    @Lob
    @Column(name = "city2")
    private String city2;

    @Lob
    @Column(name = "state2")
    private String state2;

    @Lob
    @Column(name = "pin2")
    private String pin2;

    @Lob
    @Column(name = "country2")
    private String country2;

    @Lob
    @Column(name = "report_id")
    private String reportId;

    @Lob
    @Column(name = "status")
    private String status;

    @Lob
    @Column(name = "order_id")
    private String orderId;

    @Lob
    @Column(name = "time_pulled")
    private String timePulled;

    @Column(name = "isMainApplicant")
    private Integer isMainApplicant;

    @Lob
    @Column(name = "referralCode")
    private String referralCode;

    @Lob
    @Column(name = "created")
    private String created;

    @Lob
    @Column(name = "updated")
    private String updated;

}