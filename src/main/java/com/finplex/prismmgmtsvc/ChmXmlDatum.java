package com.finplex.prismmgmtsvc;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(schema = "new_fp_schema_test",name = "chm_xml_data")
public class ChmXmlDatum implements Serializable {
    @Id
    @Column(name = "id")
    private Integer id;

    @Lob
    @Column(name = "report_id")
    private String reportId;

    @Lob
    @Column(name = "xml_response")
    private String xmlResponse;

    @Lob
    @Column(name = "created")
    private String created;

    @Lob
    @Column(name = "updated")
    private String updated;

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getXmlResponse() {
        return xmlResponse;
    }

    public void setXmlResponse(String xmlResponse) {
        this.xmlResponse = xmlResponse;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}