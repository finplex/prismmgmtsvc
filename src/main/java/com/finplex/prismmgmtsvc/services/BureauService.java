package com.finplex.prismmgmtsvc.services;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class BureauService {
    public static void writeStringToFile(String data, String fileName) throws IOException {     // Get current executing class working directory.
        File file = new File(fileName);
        try {
            //write string to file
            FileUtils.writeStringToFile(file, String.valueOf(data));
            System.out.print("Data written to file successfully.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
